# [v1.14](https://gitlab.com/bisada/OCBookmarks/tags/v1.14)

- Refactor of README.md

# [v1.13](https://gitlab.com/bisada/OCBookmarks/tags/v1.13)

- SSO initial release. 

# [v1.12](https://gitlab.com/bisada/OCBookmarks/tags/v1.12)

- New Login screen design. Show/Hide password in login screen. 

# [v1.11](https://gitlab.com/bisada/OCBookmarks/tags/v1.11)

- Added initial Drawer

# [v1.10](https://gitlab.com/bisada/OCBookmarks/tags/v1.10)

- Much waited V3 Bookmark API version fixed.

# [v1.9](https://gitlab.com/bisada/OCBookmarks/tags/v1.9)

- Can not Clear description fix!

# [v1.8](https://gitlab.com/bisada/OCBookmarks/tags/v1.8)

- V3 API Fix!
- This is an intermediate release!!
- Known issues are documented here : <https://github.com/nextcloud/bookmarks/issues/1012> 

# [v1.6](https://gitlab.com/bisada/OCBookmarks/tags/v1.6)

- Latest target version update
- Fix icons
- Gradle update.

# [v1.5](https://gitlab.com/bisada/OCBookmarks/tags/v1.5)

- Fix several translation
- Fix icons


# [v1.2](https://gitlab.com/bisada/OCBookmarks/tags/v1.2)

### Fix
- Fixed icons in menu
- fixed following / problem in log in


# [v1.1](https://gitlab.com/bisada/OCBookmarks/tags/v1.1)

### New
- Add support for owncloud (just the cooperate identity).
- add proguard (minify)
- Russian translation

### fixes
- spelling fixes
- allow to update tags


# [v1.0](https://gitlab.com/bisada/OCBookmarks/tags/v1.0)
Initial release providing general functionality.