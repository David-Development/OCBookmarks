## :link: Nextcloud Bookmarks Android App

[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
[![Android CI](https://gitlab.com/bisada/OCBookmarks/badges/master/pipeline.svg)](https://gitlab.com/bisada/OCBookmarks/-/pipelines)
[![Gitter](https://badges.gitter.im/nextcloud-bookmarks/community.svg)](https://gitter.im/nextcloud-bookmarks/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](https://gitlab.com/bisada/OCBookmarks/-/issues)

## :arrow_forward: Access
[![F-Droid Release](https://img.shields.io/f-droid/v/org.schabi.nxbookmarks)](https://f-droid.org/en/packages/org.schabi.nxbookmarks/) 

[<img src="https://raw.githubusercontent.com/stefan-niedermann/paypal-donate-button/master/paypal-donate-button.png"
      alt="Donate with PayPal"
      height="80">](https://www.paypal.me/biswajitbangalore)
[<img src="https://raw.githubusercontent.com/stefan-niedermann/DonateButtons/master/LiberaPay.png"
      alt="Donate using Liberapay"
      height="80">](https://liberapay.com/bisasda/donate)

[<img src="assets/nx/icon.png" width=160px>](/)
[![PlayStore](./assets/ps_badge.png)](https://play.google.com/store/apps/details?id=org.bisw.nxbookmarks)
[![F-Droid](./assets/fdroid_badge.png)](https://f-droid.org/packages/org.schabi.nxbookmarks/)

[<img src="assets/oc/icon.png" width=160px>](/)
[![PlayStore](./assets/ps_badge.png)](https://play.google.com/store/apps/details?id=org.bisw.nxbookmarks.owncloud)

## :eyes: Screenshots

| Multiple Accounts | SSO | Tags |  Bookmarks |
| :--: | :--: | :--: | :--: |
| ![Screenshot of list view](fastlane/metadata/android/en-US/images/phoneScreenshots/2.jpg) | ![Screenshot of edit mode](fastlane/metadata/android/en-US/images/phoneScreenshots/2.jpg)  | ![Screenshot of tag](fastlane/metadata/android/en-US/images/phoneScreenshots/4.jpg) | ![Screenshot of bookmark](fastlane/metadata/android/en-US/images/phoneScreenshots/5.jpg) |



## :rocket: Features


* Works offline 🔌
* Mark bookmarks as favorite Organize your bookmarks with labels 🔖
* Manage tags 🏷
* Translated in many languages 🌎
* Multiple accounts
* SSO : Nextcloud Single Sign On (WIP)


An Android front end for the Nextcloud [Bookmark App](https://github.com/nextcloud/bookmarks/) 
based on the new [REST API](https://github.com/nextcloud/bookmarks/#rest-api) that was introduced
by NextCloudBookmarks version [3.2.1](https://github.com/nextcloud/bookmarks/releases/tag/v3.2.1)

[<img src="assets/nx/screenshots/1.jpg" width=160px>](assets/nx/screenshots/1.jpg)
[<img src="assets/nx/screenshots/2.jpg" width=160px>](assets/nx/screenshots/2.jpg)
[<img src="assets/nx/screenshots/3.jpg" width=160px>](assets/nx/screenshots/3.jpg)
[<img src="assets/nx/screenshots/4.jpg" width=160px>](assets/nx/screenshots/4.jpg)
[<img src="assets/nx/screenshots/5.jpg" width=160px>](assets/nx/screenshots/5.jpg)


## :checkered_flag: Planned features

* [Folder Structure](https://gitlab.com/bisada/OCBookmarks/issues/17)

## :family: Join the team

  * Test the app with different devices
  * Report issues in the [issue tracker](https://gitlab.com/bisada/OCBookmarks/issues)
  * [Pick a good first issue](https://github.com/nextcloud/server/labels/good%20first%20issue) :notebook:
  * Create a [Pull Request](https://opensource.guide/how-to-contribute/#opening-a-pull-request)
  * Buy this app on [Google Play Store](https://play.google.com/store/apps/details?id=org.bisw.nxbookmarks)
  * Send me a bottle of your favorite beer :beers: :wink:

## :link: Issues
* Please note we have identified Some issues. Please look at [Issue board](https://gitlab.com/bisada/OCBookmarks/issues) before review.
* Feel free to send us a pull request.
## :link: Maintainer
* [Biswajit Das](https://gitlab.com/bisasda):@bisasda

## :link: How to compile the App

## :label: Requirements:
-------------
  1. Android Studio

:arrow_down_small: Download and install:

  1. Open cmd/terminal
  2. Navigate to your workspace
  3. Then type in: `git clone https://gitlab.com/bisada/OCBookmarks.git`
  4. Import the Project in Android Studio and start coding!

## :link: Contributors
* [Biswajit Das](https://gitlab.com/bisasda):@bisasda
* [Christian Schabesberger](https://gitlab.com/derSchabi):@derSchabi

## :link: Requirements
* [Nextcloud](https://nextcloud.com/) instance running.
* [Nextcloud Android](https://github.com/nextcloud/android) app installed (> 3.9.0)
* [Nextcloud Bookmark](https://github.com/nextcloud/bookmarks) app enabled on Instances


## :link: Contributions
* All pull requests are welcome.

[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://gitlab.com/bisada/)
